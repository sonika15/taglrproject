package com.project.taglrproject;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;


@Database(entities = {Topic.class}, version = 1)
public abstract class TopicDatabase extends RoomDatabase {

    private static TopicDatabase topicDatabase;

    public abstract TopicDao topicDao();

    public static synchronized TopicDatabase getInstance(Context context) {
        if (topicDatabase == null) {
            topicDatabase = Room.databaseBuilder(context.getApplicationContext(),
                    TopicDatabase.class, "topic_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(callback)
                    .build();
        }
        return topicDatabase;

    }

    private static RoomDatabase.Callback callback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateAsyncTask(topicDatabase).execute();
        }
    };

    private static class PopulateAsyncTask extends AsyncTask<Void, Void, Void> {
        private TopicDao topicDao;

        private PopulateAsyncTask(TopicDatabase db) {
            topicDao = db.topicDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }

}
