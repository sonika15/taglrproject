package com.project.taglrproject;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface TopicDao {

    @Insert
    void insert(Topic topic);

    @Update
    void update(Topic topic);

    @Delete
    void Delete(Topic topic);

    @Query("SELECT * FROM topic_table ORDER BY likeCount DESC")
    LiveData<List<Topic>> getAllTopics();
}
