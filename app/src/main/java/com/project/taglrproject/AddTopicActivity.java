package com.project.taglrproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddTopicActivity extends AppCompatActivity {
    EditText etTopicName, etTopicDescription;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_topic);
        etTopicName = findViewById(R.id.etTopicName);
        btnAdd = findViewById(R.id.btnAdd);
        etTopicDescription = findViewById(R.id.etTopicDescription);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidation();
            }
        });

    }

    public void doValidation() {
        if (!TextUtils.isEmpty(etTopicName.getText().toString()) && !TextUtils.isEmpty(etTopicDescription.getText().toString())) {
            Intent intent = new Intent();
            intent.putExtra(Constant.TopicName, etTopicName.getText().toString());
            intent.putExtra(Constant.TopicDescription, etTopicDescription.getText().toString());
            setResult(RESULT_OK, intent);
            finish();

        } else {
            Toast.makeText(this, "Please enter all details.", Toast.LENGTH_SHORT).show();
        }
    }
}
