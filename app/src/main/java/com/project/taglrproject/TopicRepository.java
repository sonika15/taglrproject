package com.project.taglrproject;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;

public class TopicRepository {

    private TopicDao topicDao;
    private LiveData<List<Topic>> allTopics;

    public TopicRepository(Application application) {
        TopicDatabase database = TopicDatabase.getInstance(application);
        topicDao = database.topicDao();
        allTopics = topicDao.getAllTopics();

    }


    public void insert(Topic topic) {
        new InsertAsyncTask(topicDao).execute(topic);

    }

    public void Update(Topic topic) {
        new UpdateAsyncTask(topicDao).execute(topic);

    }

    public LiveData<List<Topic>> getAllTopics() {
        return allTopics;
    }


    private static class InsertAsyncTask extends AsyncTask<Topic, Void, Void> {

        private TopicDao topicDao;

        private InsertAsyncTask(TopicDao topicDao) {
            this.topicDao = topicDao;
        }

        @Override
        protected Void doInBackground(Topic... topics) {
            topicDao.insert(topics[0]);
            return null;
        }
    }


    private static class UpdateAsyncTask extends AsyncTask<Topic, Void, Void> {

        private TopicDao topicDao;

        private UpdateAsyncTask(TopicDao topicDao) {
            this.topicDao = topicDao;
        }

        @Override
        protected Void doInBackground(Topic... topics) {
            topicDao.update(topics[0]);
            return null;
        }
    }
}
