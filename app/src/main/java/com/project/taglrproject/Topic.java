package com.project.taglrproject;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "topic_table")
public class Topic {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String topicName;

    private String topicDescription;
    private int likeCount;

    private int dislikeCount;

    public Topic(String topicName, String topicDescription, int likeCount, int dislikeCount) {
        this.topicName = topicName;
        this.topicDescription = topicDescription;
        this.likeCount = likeCount;
        this.dislikeCount = dislikeCount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getTopicDescription() {
        return topicDescription;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }
}
