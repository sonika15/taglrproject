package com.project.taglrproject;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class TopicViewModel extends AndroidViewModel {
    private TopicRepository topicRepository;
    private LiveData<List<Topic>> allTopics;


    public TopicViewModel(@NonNull Application application) {
        super(application);
        topicRepository = new TopicRepository(application);
        allTopics = topicRepository.getAllTopics();
    }

    public void insert(Topic topic) {
        topicRepository.insert(topic);
    }

    public void update(Topic topic) {
        topicRepository.Update(topic);
    }

    public LiveData<List<Topic>> getAllTopics() {
        return allTopics;
    }
}
