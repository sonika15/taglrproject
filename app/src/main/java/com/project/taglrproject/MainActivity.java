package com.project.taglrproject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int RequestCode = 1;
    RecyclerView rvList;
    FloatingActionButton btnFloating;
    private TopicViewModel topicViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvList = findViewById(R.id.rvList);
        btnFloating = findViewById(R.id.btnFloating);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setHasFixedSize(true);

        final TopicAdapter topicAdapter = new TopicAdapter();
        rvList.setAdapter(topicAdapter);

        topicViewModel = ViewModelProviders.of(this).get(TopicViewModel.class);
        topicViewModel.getAllTopics().observe(this, new Observer<List<Topic>>() {
            @Override
            public void onChanged(List<Topic> topics) {
                topicAdapter.setTopics(topics);

            }
        });
        btnFloating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddTopicActivity.class);
                startActivityForResult(intent, RequestCode);
            }
        });
        topicAdapter.setonItemClickListener(new TopicAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Topic topic, String buttonText) {
                if (buttonText.equals("dislike")) {
                    String dislikeCount = String.valueOf(topic.getDislikeCount());
                    Log.wtf("dislikeCount", dislikeCount);
                    Topic topic1 = new Topic(topic.getTopicName(), topic.getTopicDescription(), topic.getLikeCount() - 1, topic.getDislikeCount() + 1);
                    topic1.setId(topic.getId());
                    topicViewModel.update(topic1);
                } else {
                    String likeCount = String.valueOf(topic.getLikeCount());
                    Log.wtf("likeCount", likeCount);
                    Topic topic1 = new Topic(topic.getTopicName(), topic.getTopicDescription(), topic.getLikeCount() + 1, topic.getDislikeCount());
                    topic1.setId(topic.getId());
                    topicViewModel.update(topic1);
                }
            }

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCode && resultCode == RESULT_OK) {
            String topicName = data.getStringExtra(Constant.TopicName);
            String topicDescription = data.getStringExtra(Constant.TopicDescription);
            Topic topic = new Topic(topicName, topicDescription, 0, 0);
            topicViewModel.insert(topic);

        }
    }
}
